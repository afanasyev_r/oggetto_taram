const toggleMenu = () => {
    $('.js-menu-button').click( () => {
        $('.header__bottom').toggle('slow');
    })
}

const loadVideos = () => {
    const button = $('.js-load-button');

    $(button).click( (event) => {
        const id = $(event.target).attr('data-id');
        $('section[data-id=' + id + ']').find('.videos__item').show('slow');
    })
}

document.addEventListener('DOMContentLoaded', () => {
    $('.slider').slick({
        infinite: false,
        slidesToShow: 3,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        responsive: [{
            breakpoint: 769,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 426,
            settings: 'unslick'
        }]
    });

    $('[data-fancybox]').fancybox();

    toggleMenu();
    loadVideos();
});
